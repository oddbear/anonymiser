import convict from 'convict';
import dotenv from 'dotenv';

dotenv.config();

export const config = convict({
  env: {
    env: 'NODE_ENV',
    format: ['production', 'development', 'test'],
    default: 'development',
  },
  mongo: {
    uri: {
      doc: 'Mongo username',
      format: String,
      default: '',
      env: 'DB_URI',
    },
  },
}).getProperties();
