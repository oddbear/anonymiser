import { debug, error, info } from './utils/logger';
import {
  anonymiseCustomer,
  createCustomers,
  deleteAnonymisedCustomers,
  getChangedCustomers,
  getNewCustomers,
} from './services/customer.service';
import { BATCH_DOCUMENTS_LIMIT, BATCH_RESTORE_INTERVAL, DELAY_BETWEEN_GENERATIONS } from './utils/constants';
import { BatchController } from './controllers/batch.controller';
import { Customer, CustomerDocument } from './types/customer.type';
import { wait } from './utils/time';
import { ObjectId } from 'mongoose';
import { initializeMongo } from './utils/mongo';
import { config } from './config';

export const listenToChanges = async (fullReindex: boolean): Promise<void> => {
  const logCategory = 'listenToChanges';

  await initializeMongo(config.mongo.uri);

  const batchController = new BatchController<CustomerDocument, Customer, ObjectId>(
    [],
    BATCH_DOCUMENTS_LIMIT,
    BATCH_RESTORE_INTERVAL,
    (customer) => customer._id as ObjectId,
    async (customersToInsert) => {
      if (customersToInsert.length > 0) {
        await createCustomers(customersToInsert, true).catch((err) => {
          error(
            logCategory,
            err,
            'error while creating customers, probably you started the script with --full-reindex in parallel',
          );
        });

        info(logCategory, 'customers anonymisation report', {
          batchLength: customersToInsert.length,
        });
      }
    },
  );

  batchController.useMiddleware(anonymiseCustomer);

  // pooling
  do {
    const newCustomers = await getNewCustomers(batchController.getIds());

    if (newCustomers.length > 0) {
      for (const newCustomer of newCustomers) {
        batchController.push(newCustomer);
      }
    }

    const changedCustomer = await getChangedCustomers();
    if (changedCustomer.length > 0) {
      debug(logCategory, 'changed customers detected');

      const customersToDelete =
        changedCustomer.length > 1
          ? {
              $or: changedCustomer.map((customer) => ({
                _id: customer._id,
              })),
            }
          : {
              _id: changedCustomer[0]._id,
            };
      await deleteAnonymisedCustomers(customersToDelete);
    }

    await wait(DELAY_BETWEEN_GENERATIONS);
  } while (!fullReindex);

  await batchController.clearNow();
};

void (async function () {
  const args = process.argv.slice(2);
  const fullReindex = args.includes('--full-reindex');

  await listenToChanges(fullReindex);

  process.exit(0);
})();
