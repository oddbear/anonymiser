import { createCustomers, getCustomersCount } from './services/customer.service';
import { CustomerDocument } from './types/customer.type';
import { faker } from '@faker-js/faker';
import { wait } from './utils/time';
import { BATCH_DOCUMENTS_LIMIT, DELAY_BETWEEN_GENERATIONS, GENERATION_BATCH_COUNT_INTERVAL } from './utils/constants';
import { info } from './utils/logger';
import { initializeMongo } from './utils/mongo';
import { config } from './config';

const { person, location, internet, number } = faker;

export const startGeneration = async (): Promise<void> => {
  const logCategory = 'startGeneration';

  await initializeMongo(config.mongo.uri);

  let totalCustomersCount = await getCustomersCount();
  let customersCount = 0;

  const [minBatch, maxBatch] = GENERATION_BATCH_COUNT_INTERVAL;

  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition,no-constant-condition
  while (true) {
    const customers: Partial<CustomerDocument>[] = [];

    const count = number.int({ min: minBatch, max: maxBatch });

    for (let i = 0; i < count; i++) {
      customers.push({
        firstName: person.firstName(),
        lastName: person.lastName(),
        email: internet.email(),
        address: {
          line1: location.streetAddress(),
          line2: location.secondaryAddress(),
          postcode: location.zipCode(),
          city: location.city(),
          state: location.state(),
          country: location.country(),
        },
      });
    }

    customersCount += customers.length;
    totalCustomersCount += customers.length;
    if (customersCount >= BATCH_DOCUMENTS_LIMIT / 10) {
      info(logCategory, 'customers generation report', {
        customersCount: totalCustomersCount,
      });
      customersCount = 0;
    }

    await createCustomers(customers);
    await wait(DELAY_BETWEEN_GENERATIONS);
  }
};

void (async function () {
  await startGeneration();
})();
