import { model, Schema } from 'mongoose';
import { CustomerDocument } from '../types/customer.type';

export const customerSchema = {
  firstName: String,
  lastName: String,
  email: String,
  address: {
    line1: String,
    line2: String,
    postcode: String,
    city: String,
    state: String,
    country: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
};

const schema = new Schema(customerSchema);

export const customerModel = model<CustomerDocument>('customers', schema);
