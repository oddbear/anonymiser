import { model, Schema } from 'mongoose';
import { CustomerDocument } from '../types/customer.type';

export const customerAnonymisedSchema = {
  firstName: String,
  lastName: String,
  email: String,
  address: {
    line1: String,
    line2: String,
    postcode: String,
    city: String,
    state: String,
    country: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
};

const schema = new Schema(customerAnonymisedSchema);

export const customerAnonymisedModel = model<CustomerDocument>('customers_anonymised', schema);
