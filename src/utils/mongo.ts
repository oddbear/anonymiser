import mongoose from 'mongoose';
import { MongoError } from 'mongodb';
import { error, info } from './logger';

export const initializeMongo = async (uri: string): Promise<void> => {
  const logCategory = 'initializeMongo';

  await mongoose
    .connect(uri)
    .then(async () => {
      info(logCategory, 'mongo connected');
    })
    .catch((err: MongoError) => {
      error(logCategory, err, 'error while connecting to mongo');
      throw err;
    });
};
