import { createHash } from 'node:crypto';

export const anonymiseString = (input: string): string => {
  return createHash('md5').update(input).digest('hex').slice(0, 8);
};

export const anonymiseEmail = (email: string): string => {
  const [name, domain] = email.split('@');
  return `${anonymiseString(name)}@${domain}`;
};
