import { Document } from 'mongoose';

export interface CustomerAddress {
  line1: string;
  line2: string;
  postcode: string;
  city: string;
  state: string;
  country: string;
}

export interface Customer {
  firstName: string;
  lastName: string;
  email: string;
  address: CustomerAddress;
  createdAt: Date;
}

export type CustomerDocument = Customer & Document;
