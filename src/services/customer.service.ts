import { Customer, CustomerDocument } from '../types/customer.type';
import { anonymiseEmail, anonymiseString } from '../utils/anonymise';
import { customerAnonymisedModel } from '../models/customersAnonymised.model';
import { customerModel } from '../models/customers.model';
import { FilterQuery, ObjectId } from 'mongoose';

export const createCustomers = async (
  customers: Partial<Customer>[],
  useAnonymisedCollection = false,
): Promise<CustomerDocument[]> => {
  return (useAnonymisedCollection ? customerAnonymisedModel : customerModel).insertMany(customers);
};

export const getNewCustomers = async (usedIds: ObjectId[]): Promise<CustomerDocument[]> => {
  const pipeline = [
    {
      $match: {
        _id: {
          $nin: usedIds,
        },
      },
    },
    {
      $lookup: {
        from: `${customerAnonymisedModel.modelName}s`,
        localField: '_id',
        foreignField: '_id',
        as: 'joined_docs',
      },
    },
    {
      $match: {
        joined_docs: {
          $size: 0,
        },
      },
    },
  ];

  return customerModel.aggregate<CustomerDocument>(pipeline);
};

const getHashingOperation = (fieldName: string): object => {
  return {
    $function: {
      body: `function(field) {
        if (!field) {
          return '';
        }
        if (field.indexOf('@') > -1) {
          field = hex_md5(field.slice(0, field.indexOf('@'))).substr(0, 8) + field.slice(field.indexOf('@'));
          return field;
        }
        return hex_md5(field).substr(0, 8);
      }`,
      args: [`$${fieldName}`],
      lang: 'js',
    },
  };
};

export const getChangedCustomers = async (): Promise<CustomerDocument[]> => {
  const pipeline = [
    {
      $lookup: {
        from: `${customerAnonymisedModel.modelName}s`,
        localField: '_id',
        foreignField: '_id',
        as: 'anonymised',
      },
    },
    {
      $unwind: {
        path: '$anonymised',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $match: {
        anonymised: {
          $exists: true,
        },
      },
    },
    {
      $addFields: {
        hashed: {
          firstName: getHashingOperation('firstName'),
          lastName: getHashingOperation('lastName'),
          address: {
            line1: getHashingOperation('address.line1'),
            line2: getHashingOperation('address.line2'),
            postcode: getHashingOperation('address.postcode'),
          },
          email: getHashingOperation('email'),
        },
      },
    },
    {
      $redact: {
        $cond: {
          if: {
            $or: [
              { $ne: [getHashingOperation('firstName'), '$anonymised.firstName'] },
              { $ne: [getHashingOperation('lastName'), '$anonymised.lastName'] },
              { $ne: [getHashingOperation('address.line1'), '$anonymised.address.line1'] },
              { $ne: [getHashingOperation('address.line2'), '$anonymised.address.line2'] },
              { $ne: [getHashingOperation('address.postcode'), '$anonymised.address.postcode'] },
              { $ne: [getHashingOperation('email'), '$anonymised.email'] },
              { $ne: ['$address.city', '$anonymised.address.city'] },
              { $ne: ['$address.state', '$anonymised.address.state'] },
              { $ne: ['$address.country', '$anonymised.address.country'] },
            ],
          },
          then: '$$DESCEND',
          else: '$$PRUNE',
        },
      },
    },
  ];

  return customerModel.aggregate<CustomerDocument>(pipeline);
};

export const anonymiseCustomer = (customer: CustomerDocument): Customer & { _id: ObjectId } => {
  const [firstName, lastName, addressLine1, addressLine2, addressPostcode] = [
    customer.firstName,
    customer.lastName,
    customer.address.line1,
    customer.address.line2,
    customer.address.postcode,
  ].map(anonymiseString);

  return {
    _id: customer._id,
    firstName,
    lastName,
    email: anonymiseEmail(customer.email),
    address: {
      ...customer.address,
      line1: addressLine1,
      line2: addressLine2,
      postcode: addressPostcode,
    },
    createdAt: customer.createdAt,
  };
};

export const deleteAnonymisedCustomers = async (filter: FilterQuery<CustomerDocument>): Promise<void> => {
  await customerAnonymisedModel.deleteMany(filter);
};

export const getCustomersCount = async (): Promise<number> => {
  return customerModel.countDocuments();
};
