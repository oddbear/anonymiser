import { ObjectId } from 'mongoose';

type Middleware<T, I> = (item: T) => I;

export class BatchController<Parent extends Child, Child, Id> {
  private timer: NodeJS.Timeout | null = null;
  private middleware: Middleware<Parent, Child> | null = null;
  private ids: Id[] = [];

  constructor(
    private collection: Child[],
    private readonly limit: number,
    private readonly restoreInterval: number,
    private readonly getId: (item: Parent) => Id,
    private readonly beforeClear: (collection: Child[]) => Promise<void>,
  ) {}

  public getIds(): Id[] {
    return this.ids;
  }

  public useMiddleware(callback: Middleware<Parent, Child>): void {
    this.middleware = callback;

    const collection = this.collection;
    this.collection = [];
    for (const item of collection) {
      this.push(item as Parent);
    }
  }

  public push(element: Parent): void {
    this.ids.push(this.getId(element));

    let preparedElement: Child = element;
    if (this.middleware != null) {
      preparedElement = this.middleware(element);
    }

    this.collection.push(preparedElement);

    if (this.collection.length >= this.limit) {
      this.clear();
    }

    this.startTimer();
  }

  public clearNow(): Promise<void> {
    return this.clear(true);
  }

  private startTimer(): void {
    if (this.timer == null) {
      this.timer = setTimeout(() => {
        this.clear();
      }, this.restoreInterval);
    }
  }

  private clear(async: true): Promise<void>;
  private clear(async?: false): void;
  private clear(async?: boolean): void | Promise<void> {
    const collection = this.collection;

    this.collection = [];
    this.ids = [];

    if (this.timer != null) {
      clearTimeout(this.timer);
      this.timer = null;

      this.startTimer();
    }

    if (async === true) {
      return new Promise((resolve, reject) => {
        this.beforeClear(collection).then(resolve).catch(reject);
      });
    }
    void this.beforeClear(collection);
  }
}
